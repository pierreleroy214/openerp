# openERP

Le but de ce projet est de créer un logiciel de gestion des ressources (Entrerprise Ressource Planning) pour des sociétés de mécanique.

Pour une accessibilité maximale, ce logiciel est sous forme d'une application web MERN.

## MERN

Mongodb Express React NodeJS

### Structure

Le projet est séparé en 2 parties principales: Une partie frontEnd et une partie backEnd

la racine du projet est initisalisée avec npm afin de pouvoir utiliser:

- concurrently: permet de demmarrer en même temps le backend et le frontend, et le redemarrage automatique des deux lors des changements. Utile pour la phase de développement seulement
- prettier: pour la mise en forme des fichiers

#### .gitignore

Le fichier .gitignore contient les éléments devant être ingoré par git.
Il contient donc les dossier node_modules de la racine, ainsi que ceux des dossiers frontend et backend

#### FrontEnd

La partie frontend de cette application utilise le framework NEXT.JS.

#### BackEnd

La partie backend de cette application utilise les modules:
- Express: fournit les mecanismes HTTP
- Mongoose: pour communiquer avec la base de données
- Nodemon: pour redemmarrer automatiquement l'application lors du developpement. Non utile pour l'utlisation finale

La partie backend est disponible sur le port 5000, la base de données est disponible sur le port 27017.

Les schemas de chaque collections de la base de données sont définis dans les fichiers .model.js présent dans le dossier models
Pour chaque collection, un fichier .ctrl.js est créé dans le dossier controllers afin d'y ajouter les fonctions CRUD
Chaque fonction est reliée à une adresse par les fichiers routes.js défini pour chaque collection
un fichier index.routes.js regroupe toutes les routes.



## Roadmap

### FrontEnd

- [x] Créer la trame de base avec NEXT
- [x] Créer une page de login
- [x] Créer une page avec side et top bar
- [x] Créer le memu dans la sidebar
- [ ] Créer la pages article et afficher les articles de la base de donnée
  - [x] afficher un tableau avec react-table
  - [ ] ajouter axios.js pour faire des requêtes au server back-end
  - [ ] afficher les données retournées par la requête axios dans le tableau
- [ ] Créer un form pour ajouter des articles dans la base de donnée
- [ ] Créer une page de login en 2 pages, 1 pour l'identifiant l'autre pour le mot de passe

### BackEnd

- [x] Créer le dossier backend
- [x] Créer les schemas(models) des collections articles, gammes, machines, lancements et matière
- [x] Créer les routes, et controllers des articles, gammes, machines, lancements et matières


####Schemas des différentes collections

Article

```json
{
code: String,
designation: String,
dateDeCreation: Date,
createur: String,
type: String,
numeroDePlan: String,
indiceDePlan: String,
etatDeValidation: String,
uniteDeGestion: String,
quantiteEconomique: Number,
stockDeSecurite: Number,
prixDeVente: Number
gammePrincipale: gamme.id,
gammeSecondaire: [gamme.id]
}
```

Machine
```json
{
numero: Number,
nom: String
}
```

User
```json
{
  nom: String,
  prenom; String,
  email: String,
  password:String
}
```

Matière
```json
{
  code: String,
  designation: String
}
```


###Définition Phase, gamme. lancement
Phase
```json
{
  machine: machine.id,
  tempsReglage: number,
  tempsProductionUnitaire: number,
}
```

Gamme
```json
{
  code: String,
  designation: String,
  estPrincipale: Boolean,
  operations: [
  {
    machine: machine.id,
    tempsReglage: Number,
    tempsProductionUnitaire: Number
      composants:[
      {
        codeArticle: article.code,
        designationArticle: article.designation,
        quantite: Number
      }
      ]
  },
  ]
}
```

Lancement
```json
{
  numeroDeLancement: String,
  article: article.code,
  numeroDePlan: article.numeroDePlan,
  indiceDePlan: article.indiceDePlan,
  quantiteAFabriquer: Number,
  gamme: [
  {
    machine: gamme.operations[0].machine,
    tempsDeReglage: gamme.operations[0].tempsDeReglage,
    tempsProductionUnitaire: gamme.operation[0].tempsProductionUnitaire,
    compsants: [
    {
      codearticle: article.code,
      designation: article.designation,
      quantite: Number
    }
    ]
    etatAvancement: String,
    pointages: [
    {
      operateur: String,
      datePointage: Date,
      dateProduction: date,
      tempsReglage: Number,
      tempsProduction: Number,
      NombrePieceComforme: Number,
      NombrePieceNonConforme: Number
    }
    ]
  }
  ]
}
```

