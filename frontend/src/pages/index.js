import {
  Flex,
  Heading,
  Input,
  Button,
  useColorMode,
  useColorModeValue,
} from "@chakra-ui/react";
import Link from "next/link";

function HomePage() {
  const { toggleColorMode } = useColorMode();
  const formBackground = useColorModeValue("gray.100", "gray.700");
  return (
    <Flex height="100vh" alignItems="center" justifyContent="center">
      <Flex direction="column" background={formBackground} p={12} rounded={6}>
        <Heading mb={6}>Log In</Heading>
        <Input placeholder="identifiant" variant="filled" mb={3} type="email" />
        <Input
          placeholder="***********"
          variant="filled"
          mb={6}
          type="password"
        />
        <Button mb={6} colorScheme="teal">
          <Link href="/home">
            <a>Connexion</a>
          </Link>
        </Button>
        <Button onClick={toggleColorMode}>Toggle color</Button>
      </Flex>
    </Flex>
  );
}

export default HomePage;
