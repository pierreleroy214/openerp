import React, { useEffect } from 'react';
import { Button, Flex, Box, Grid, GridItem, Table, Thead,Tbody,Tr,Th,Td,Checkbox,Drawer,DrawerBody,DrawerFooter,DrawerHeader,DrawerOverlay,DrawerContent,DrawerCloseButton,useDisclosure,Input,FormControl,FormLabel} from "@chakra-ui/react";
import {useTable,useSortBy} from 'react-table';
import { FiChevronUp, FiChevronDown, FiPlus } from 'react-icons/fi'
import TopBar from "../components/TopBar";
import SideBar from "../components/SideBar";

export const getStaticProps = async () => {
	const res = await fetch(`http://localhost:5000/lancement`)
	const lancements = await res.json()

	return {
		props: {
			lancements
		}
	}
}


function Home(lancements) {
  const data = React.useMemo(
    () => lancements.lancements,
    []
  )
  const columns = React.useMemo(
    () => [
      {
        Header: 'Numero de lancement',
        accessor: 'numero_lancement'
      },
      {
        Header: 'Ref. Article',
        accessor: 'article_id'
      },
      {
        Header: 'Quantite',
        accessor: 'quantite'
      },
    ],
    []
  )
  const tableInstance = useTable({columns, data },useSortBy);
  const {getTableProps, getTableBodyProps, headerGroups, rows, prepareRow} = tableInstance;
  const firstPageRows = rows.slice(0,12)
	const {isOpen, onOpen, onClose } = useDisclosure();
	const btnAjouter = React.useRef();
  return (
    <Flex minH="100vh">
      <SideBar />
      <Box flex="1">
        <TopBar />
        <Box p="4" h='95vh'>
          <Button ref={btnAjouter} colorScheme='teal' size="lg" leftIcon={<FiPlus/>} onClick={onOpen}>
            Ajouter
          </Button>
		<Drawer
			isOpen={isOpen}
			size = 'xl'
			placement='right'
			onClose={onClose}
			finalFocusRef={btnAjouter}
		>
			<DrawerOverlay />
			<DrawerContent>
				<DrawerCloseButton />
				<DrawerHeader>Ajouter un lancement</DrawerHeader>
				<DrawerBody>
					<FormControl>
						<FormLabel htmlFor='code_article'>Code article</FormLabel>
						<Input id='code_article' type='text'/>
					</FormControl>
					<FormControl>
						<FormLabel htmlFor='quantite'>Quantite</FormLabel>
						<Input id='quantite'/>
					</FormControl>
				</DrawerBody>
				<DrawerFooter>
					<Button variant='outline' mr={3} onClick={onClose}>
						Cancel
					</Button>
					<Button colorScheme='teal'>Save</Button>
				</DrawerFooter>
			</DrawerContent>
		</Drawer>

          <Table>
            <Thead>
              {
                headerGroups.map(headerGroup => (
                  <Tr {...headerGroup.getHeaderGroupProps()}>
                    {
                      headerGroup.headers.map(column =>(
                        <Th {...column.getHeaderProps(column.getSortByToggleProps())}>
                          {column.render('Header')}
                          <span>
                            {column.isSorted ? column.isSortedDesc ? <FiChevronDown/> : <FiChevronUp/> : ''}
                          </span>
                        </Th>
                      ))}
                  </Tr>
                ))}
            </Thead>
            <Tbody {...getTableBodyProps()}>
              {
                firstPageRows.map(row => {
                  prepareRow(row)
                  return (
                    <Tr {...row.getRowProps()}>
                      {row.cells.map(cell => {
                        return (
                          <Td {...cell.getCellProps()}>
                            {cell.render('Cell')}
                          </Td>
                        )
                      })}
                    </Tr>
                  )
              })}
            </Tbody>
          </Table>
        </Box>
      </Box>
    </Flex>
  );
}

export default Home;
