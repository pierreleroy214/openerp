import { Flex, Box, Grid, GridItem } from "@chakra-ui/react";
import TopBar from "../components/TopBar";
import SideBar from "../components/SideBar";

function Home() {
  return (
    <Flex minH="100vh">
      <SideBar />
      <Box flex="1">
        <TopBar />
        <Grid
          h="95vh"
          templateRows="repeat(2,1fr)"
          templateColumns="repeat(3,1fr)"
          gap={4}
          p={4}
        >
          <GridItem colSpan={1} bg="teal.600" borderRadius="lg" />
          <GridItem colSpan={1} bg="teal.600" borderRadius="lg" />
          <GridItem colSpan={1} bg="teal.600" borderRadius="lg" />
          <GridItem colSpan={2} rowSpan={1} bg="teal.600" borderRadius="lg" />
          <GridItem colSpan={1} bg="teal.600" borderRadius="lg" />
        </Grid>
      </Box>
    </Flex>
  );
}

export default Home;
