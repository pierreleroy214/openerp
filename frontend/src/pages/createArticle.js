import React from 'react';
import { Select,FormControl, FormLabel,Input,Flex,Box,Button,Checkbox } from "@chakra-ui/react";
import TopBar from "../components/TopBar";
import SideBar from "../components/SideBar";

function Home() {
  return (
    <Flex minH="100vh">
      <SideBar />
      <Box flex="1">
        <TopBar />
        <Box p="4" h='95vh'>
          <FormControl>
            <FormLabel htmlFor='code'>Code</FormLabel>
            <Input id='code' type='text'/>
          </FormControl>
          <FormControl>
            <FormLabel htmlFor='designation'>Désignation</FormLabel>
            <Input id='designation' type='text'/>
          </FormControl>
          <Select placeholder='Selctionner le type'>
            <option value='pièce'>Pièce</option>
            <option value='matière'>Matière</option>
            <option value='outillage'>Outillage</option>
          </Select>
          <FormControl>
            <FormLabel htmlFor='numeroDePlan'>Numéro de plan</FormLabel>
            <Input id='numeroDePlan' type='text'/>
          </FormControl>
        </Box>
      </Box>
    </Flex>
  );
}

export default Home;
