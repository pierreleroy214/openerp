import React, { useEffect } from 'react';
import { Button, Flex, Box, Grid, GridItem, Table, Thead,Tbody,Tr,Th,Td,Checkbox } from "@chakra-ui/react";
import {useTable,useSortBy} from 'react-table';
import { FiChevronUp, FiChevronDown, FiPlus } from 'react-icons/fi'
import TopBar from "../components/TopBar";
import SideBar from "../components/SideBar";

export const getStaticProps = async () => {
	const res = await fetch(`http://localhost:5000/article`)
	const articles = await res.json()

	return {
		props: {
			articles
		}
	}
}


function Home(articles) {
  const data = React.useMemo(
    () => articles.articles,
    []
  )
  const columns = React.useMemo(
    () => [
      {
        Header: 'Code',
        accessor: 'code'
      },
      {
        Header: 'Désignation',
        accessor: 'designation'
      },
      {
        Header: 'Stock',
        accessor: 'stock'
      },
    ],
    []
  )
  const tableInstance = useTable({columns, data },useSortBy);
  const {getTableProps, getTableBodyProps, headerGroups, rows, prepareRow} = tableInstance;
  const firstPageRows = rows.slice(0,12)
  return (
    <Flex minH="100vh">
      <SideBar />
      <Box flex="1">
        <TopBar />
        <Box p="4" h='95vh'>
          <Button colorScheme='teal' size="lg" leftIcon={<FiPlus/>}>
            Ajouter
          </Button>
          <Table>
            <Thead>
              {
                headerGroups.map(headerGroup => (
                  <Tr {...headerGroup.getHeaderGroupProps()}>
                    {
                      headerGroup.headers.map(column =>(
                        <Th {...column.getHeaderProps(column.getSortByToggleProps())}>
                          {column.render('Header')}
                          <span>
                            {column.isSorted ? column.isSortedDesc ? <FiChevronDown/> : <FiChevronUp/> : ''}
                          </span>
                        </Th>
                      ))}
                  </Tr>
                ))}
            </Thead>
            <Tbody {...getTableBodyProps()}>
              {
                firstPageRows.map(row => {
                  prepareRow(row)
                  return (
                    <Tr {...row.getRowProps()}>
                      {row.cells.map(cell => {
                        return (
                          <Td {...cell.getCellProps()}>
                            {cell.render('Cell')}
                          </Td>
                        )
                      })}
                    </Tr>
                  )
              })}
            </Tbody>
          </Table>
        </Box>
      </Box>
    </Flex>
  );
}

export default Home;
