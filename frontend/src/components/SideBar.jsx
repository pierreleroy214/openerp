import Link from 'next/link'
import { Flex, Box, Text, useColorModeValue } from "@chakra-ui/react";
import {
  FiPackage,
  FiShoppingCart,
  FiDatabase,
  FiActivity,
  FiBarChart2,
} from "react-icons/fi";
import NavItem from "./NavItem";

function SideBar() {
  return (
    <Box
      w="55"
      borderRight="1px"
      borderRightColor={useColorModeValue("gray.200", "gray.700")}
    >
      <Flex h="20" alignItems="center" mx="8" justifyContent="space-between">
        <Text fontSize="2xl" fontFamily="monospace" fontWeight="bold">
          OpenERP
        </Text>
      </Flex>
      <NavItem href="/home" key="Dashboard" icon={FiBarChart2}>
        Dashboard
      </NavItem>
      <NavItem href="/article" key="Articles" icon={FiDatabase}>
        Articles
      </NavItem>
      <NavItem href="/lancement" key="Production" icon={FiActivity}>
        Production
      </NavItem>
      <NavItem href="/home" key="Achats" icon={FiShoppingCart}>
        Achats
      </NavItem>
      <NavItem href="/home" key="Stock" icon={FiPackage}>
        Stock
      </NavItem>
    </Box>
  );
}

export default SideBar;
