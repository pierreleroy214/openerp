const express = require('express');
const mongoose = require('mongoose');

const app = express();
let port = 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static('public'));
app.use(require('./routes/index.routes'));

mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******
	
const lancementSchema = new mongoose.Schema({
	numero_lancement: {
		type: String,
		required: true,
		unique: true
	},
	article_id: String,
	quantite: Number,
	etat: String

});

const machineSchema = new mongoose.Schema({
	numero: {
		type: Number,
		required: true,
		unique: true
	},
  	nom: String
});
//  
// const userSchema = new mongoose.Schema({
//  	nom: String,
//  	prenom: String,
//  	email: String,
//  	password: String
// });
//  
 const nuanceMatiereSchema = new mongoose.Schema({
  	code: String,
  	designation: String
 });

const gammeSchema = new mongoose.Schema({
	numero: Number,
	designation: String,
	operations: [{numero: Number, machine_id: String, description: String, tps_reglage: Number, tps_fabrication: Number}],
	articles: [{article_id: String}]
})

// ******* Creating models *******
const Lancement = mongoose.model('Lancement', lancementSchema);
const Machine = mongoose.model('Machine', machineSchema);
// const User = mongoose.model('User', userSchema);
const NuanceMatiere = mongoose.model('NuanceMatiere', nuanceMatiereSchema);
const Gamme = mongoose.model('Gamme', gammeSchema);

// ******* Creating some documents if colelctions empty ******

Machine.countDocuments({}, function(err, count){
	if(err){
		console.log(err);
	} else {
		if (count === 0){
			console.log("Pas de machine dans la base, creation de 2 machines pour test");
			let machine = new Machine ({
				numero: 833,
				nom: "NH 833"
			});
			machine.save();
			let machine2 = new Machine ({
				numero: 831,
				nom : "NH 831"
			});
			machine2.save();
		}
	}
})



// ******* Routing *******

/* Ajouter un article - POST - /articles */
router.post('/', m.checkContent, Article.saveOne)


app.get('/',(req,res)=>{
	res.send("Open source ERP")
});

app.listen(port, ()=> {
	console.log('Serveur up and running on port ' + port)
});
