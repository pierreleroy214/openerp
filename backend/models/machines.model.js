const mongoose = require('mongoose');

const machineSchema = new mongoose.Schema({
	code: {
		type: Number,
		required: true,
		unique: true
	},
	designation: String	
});

const Machine = mongoose.model('Machine', machineSchema);

module.exports = {
	Machine
}
