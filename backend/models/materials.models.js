const mongoose = require('mongoose');

const materialSchema = new mongoose.Schema({
	code: String,
	designation: String
});

const Material = mongoose.model('Material', materialSchema);

module.exports = {
	Material
}
