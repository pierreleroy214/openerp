const mongoose = require('mongoose');

const lancementSchema = new mongoose.Schema({
	numero_lancement: {
		type: String,
		required: true,
		unique: true
	},
	article_id: String,
	quantite: Number,
	etat: String
});

const Lancement = mongoose.model('Lancement', lancementSchema);

module.exports = {
	Lancement
}
