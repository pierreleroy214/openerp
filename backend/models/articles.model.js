const mongoose = require('mongoose');

const articleSchema = new mongoose.Schema({
	code: {
		type: String,
		required: true,
		unique: true
	},
	designation: String,
	stock: Number,
	qte_en_commande: Number
//	dateDeCreation: Date,
//	createur: String,
//	type: String,
//	numeroDePlan: String,
//	indiceDePlan: String,
//	etatDeValiadtion: String,
//	uniteDeGestion: String,
//	quantiteEconimique: Number,
//	stockDeSecurite: Number,
//	prixDeVente: Number,
//	gammePrincipale: String
});

const Article = mongoose.model('Article', articleSchema);

module.exports = {
	Article
}
