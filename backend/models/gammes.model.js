const mongoose = require('mongoose');

const gammeSchema = new mongoose.Schema({
	code: String,
	designation: String,
	operations: [{
		numero: Number,
		machine_id: String,
		description: String,
		tps_reglage: Number,
		tps_fabrication: Number
	}],
	articles: [{article_id: String}]
});

const Gamme = mongoose.model('Gamme', gammeSchema);

module.exports = {
	Gamme
}
