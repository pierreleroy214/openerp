const mongoose = require('mongoose');
const fs = require('fs');

let rawdata = fs.readFileSync('csvjson.json');
let articles = JSON.parse(rawdata);


mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******

const articleSchema = new mongoose.Schema({
	code: String,
	designation: String,
	stock: Number,
	qte_en_commande: Number,
	qte_par_jeu: Number
});

// ******* Creating models *******
const Article = mongoose.model('Article', articleSchema);


articles.forEach(element => {

	const regexMorsLadner = /\b\d{10}\D\d{2}\b/;
	const regexMorsBison = /\bMB\d{2}-/;

	let estUnMors = regexMorsLadner.test(element["Code article"]);
	console.log(element["Code article"] + " est un mors: " + estUnMors);

	let qteParJeu = 1;

	if ( regexMorsLadner.test(element["Code article"]) ) {
		console.log("nombre de mors par jeu: " + element["Code article"][9]);
		qteParJeu = parseInt(element["Code article"][9]);
	}else if ( regexMorsBison.test(element["Code article"]) ) {
 		console.log("MORS BISON");
 		qteParJeu = parseInt(element["Code article"][3]);
 		qteParJeu = qteParJeu - 2;
 		console.log(qteParJeu);
 	}
 
	const nouvelArticle = new Article({
		code: element["Code article"],
		designation: element["Libellé"],
		stock: parseInt(element["Stock réel"]),
		qte_en_commande: element["Quantité commandée"],
		qte_par_jeu: qteParJeu
	})
	nouvelArticle.save();
	console.log("Article ajoute: " + element["Code article"]);
	
});
