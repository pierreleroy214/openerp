const mongoose = require('mongoose');
const fs = require('fs');



mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******

const articleSchema = new mongoose.Schema({
	code: String,
	designation: String,
	stock: Number,
	qte_en_commande: Number,
	qte_par_jeu: Number
});

const lancementSchema = new mongoose.Schema({
	numero_lancement: Number,
	article_code: String,
	designation: String,
	quantite: Number,
	etat: String

});

// ******* Creating models *******
const Article = mongoose.model('Article', articleSchema);
const Lancement = mongoose.model('Lancement', lancementSchema);



async function extractLancements() {
	try{
		const lancements = await Lancement.find();
		for (const lancement of lancements) {
			let content = "";
			content = lancement.numero_lancement + ";" + lancement.article_code + ";" + lancement.designation + ";" + lancement.quantite + ";\r\n";
			fs.writeFile('lancementsExtract.csv', content, { flag: 'a+' }, err =>{});
		}
	}catch(err){
		console.log(err);
	}
};

extractLancements();
console.log("Extraction terminée")
