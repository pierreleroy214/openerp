const Machine = require('../models/machines.model.js').Machine

function saveOne(req,res){
	const newMachine = new Machine(req.body);

	return newMachine
		.save()
		.then((result) => {
			res
				.status(201)
				.json({ message: `machine ${result._id} created`, content: result  })
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({message: 'this machine code already existing'})
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			) {
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
		})
}

function findAll(req, res) {
	return Machine.find()
		.exec()
		.then((result) => {
			if (result.length > 0) {
				res.json(result)
			} else {
				res.status(202).json({ message: 'no machines available' })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function findById(req,res){
	const id = req.params.id

	return Machine.findById(id)
		.exec()
		.then((result) => {
			if(result){
				res.json(result)
			} else {
				res.status(404).json({ message: message.machine.notFound(id) })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function updateById(req,res) {
	const id = req.params.id

	return Machine.findByIdAndUpdate(id, req.body, {new: true, runValidators: true})
		.then((result) => {
			if (result) {
				res.json({ message: `machine ${id} updated`, content: result })
			} else {
				res.status(404).json({ message: `machine ${id} not found ` })
			}
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({ message: 'this machine code already existing' })
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			){
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
			
		})
}

function deleteById(req, res) {
	const id = req.params.id

	return Machine.findByIdAndRemove({ _id: id })
		.then((result) => {
			if(result) {
				res.json({ message: `machine ${id} deleted` })
			} else {
				res.status(404).json({ message: `machine ${id} not found` })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

module.exports = {
	saveOne,
	findAll,
	findById,
	updateById,
	deleteById
}
