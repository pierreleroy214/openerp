const Material = require('../models/materials.models.js').Material

function saveOne(req,res){
	const newMaterial = new Material(req.body);

	return newMaterial
		.save()
		.then((result) => {
			res
				.status(201)
				.json({ message: `material ${result._id} created`, content: result  })
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({message: 'this material code already existing'})
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			) {
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
		})
}

function findAll(req, res) {
	return Material.find()
		.exec()
		.then((result) => {
			if (result.length > 0) {
				res.json(result)
			} else {
				res.status(202).json({ message: 'no materials available' })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function findById(req,res){
	const id = req.params.id

	return Material.findById(id)
		.exec()
		.then((result) => {
			if(result){
				res.json(result)
			} else {
				res.status(404).json({ message: message.material.notFound(id) })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function updateById(req,res) {
	const id = req.params.id

	return Material.findByIdAndUpdate(id, req.body, {new: true, runValidators: true})
		.then((result) => {
			if (result) {
				res.json({ message: `material ${id} updated`, content: result })
			} else {
				res.status(404).json({ message: `material ${id} not found ` })
			}
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({ message: 'this material code already existing' })
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			){
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
			
		})
}

function deleteById(req, res) {
	const id = req.params.id

	return Material.findByIdAndRemove({ _id: id })
		.then((result) => {
			if(result) {
				res.json({ message: `material ${id} deleted` })
			} else {
				res.status(404).json({ message: `material ${id} not found` })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

module.exports = {
	saveOne,
	findAll,
	findById,
	updateById,
	deleteById
}
