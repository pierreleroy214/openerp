const Gamme = require('../models/gammes.model.js').Gamme

function saveOne(req,res){
	const newGamme = new Gamme(req.body);

	return newGamme
		.save()
		.then((result) => {
			res
				.status(201)
				.json({ message: `gamme ${result._id} created`, content: result  })
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({message: 'this gamme code already existing'})
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			) {
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
		})
}

function findAll(req, res) {
	return Gamme.find()
		.exec()
		.then((result) => {
			if (result.length > 0) {
				res.json(result)
			} else {
				res.status(202).json({ message: 'no gammes available' })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function findById(req,res){
	const id = req.params.id

	return Gamme.findById(id)
		.exec()
		.then((result) => {
			if(result){
				res.json(result)
			} else {
				res.status(404).json({ message: message.gamme.notFound(id) })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function updateById(req,res) {
	const id = req.params.id

	return Gamme.findByIdAndUpdate(id, req.body, {new: true, runValidators: true})
		.then((result) => {
			if (result) {
				res.json({ message: `gamme ${id} updated`, content: result })
			} else {
				res.status(404).json({ message: `gamme ${id} not found ` })
			}
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({ message: 'this gamme code already existing' })
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			){
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
			
		})
}

function deleteById(req, res) {
	const id = req.params.id

	return Gamme.findByIdAndRemove({ _id: id })
		.then((result) => {
			if(result) {
				res.json({ message: `gamme ${id} deleted` })
			} else {
				res.status(404).json({ message: `gamme ${id} not found` })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

module.exports = {
	saveOne,
	findAll,
	findById,
	updateById,
	deleteById
}
