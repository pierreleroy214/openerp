const Article = require('../models/articles.model.js').Article

function saveOne(req,res){
	const newArticle = new Article(req.body);

	return newArticle
		.save()
		.then((result) => {
			res
				.status(201)
				.json({ message: `article ${result._id} created`, content: result  })
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({message: 'this article code already existing'})
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			) {
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
		})
}

function findAll(req, res) {
	return Article.find()
		.exec()
		.then((result) => {
			if (result.length > 0) {
				res.json(result)
			} else {
				res.status(202).json({ message: 'no articles available' })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function findById(req,res){
	const id = req.params.id

	return Article.findById(id)
		.exec()
		.then((result) => {
			if(result){
				res.json(result)
			} else {
				res.status(404).json({ message: message.article.notFound(id) })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

function updateById(req,res) {
	const id = req.params.id

	return Article.findByIdAndUpdate(id, req.body, {new: true, runValidators: true})
		.then((result) => {
			if (result) {
				res.json({ message: `article ${id} updated`, content: result })
			} else {
				res.status(404).json({ message: `article ${id} not found ` })
			}
		})
		.catch((err) => {
			if (err.code === 11000) {
				res.status(409).json({ message: 'this article code already existing' })
			} else if (
				err.errors &&
				Object.keys(err.errors).length > 0 &&
				err.name === 'ValidationError'
			){
				res.status(422).json({ message: err.message })
			} else {
				res.status(500).json(err)
			}
			
		})
}

function deleteById(req, res) {
	const id = req.params.id

	return Article.findByIdAndRemove({ _id: id })
		.then((result) => {
			if(result) {
				res.json({ message: `article ${id} deleted` })
			} else {
				res.status(404).json({ message: `article ${id} not found` })
			}
		})
		.catch((err) => {
			res.status(500).json(err)
		})
}

module.exports = {
	saveOne,
	findAll,
	findById,
	updateById,
	deleteById
}
