const express = require('express');
const mongoose = require('mongoose');


const app = express();
let port = 5000;

app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(require('./routes/index.routes'));

mongoose.connect('mongodb://localhost:27017/erp');

// ******* Routing *******

app.get('/',(req,res)=>{
	res.send("Open source ERP")
});

app.listen(port, ()=> {
	console.log('Serveur up and running on port ' + port)
});
