const mongoose = require('mongoose');
const fs = require('fs');

let rawdata = fs.readFileSync('csvjson.json');
let articles = JSON.parse(rawdata);


mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******

const articleSchema = new mongoose.Schema({
	code: String,
	designation: String,
	stock: Number,
	qte_en_commande: Number
});

// ******* Creating models *******
const Article = mongoose.model('Article', articleSchema);


articles.forEach(element => {
	Article.findOneAndUpdate({code: element["Code article"]},{$set: {stock: parseInt(element["Stock réel"]), qte_en_commande: element["Quantité commandée"]}},function() {console.log("update an article")})
			
	});

console.log("Update done.");
