const mongoose = require('mongoose');
const fs = require('fs');

let rawdata = fs.readFileSync('csvjson.json');
let articles = JSON.parse(rawdata);


mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******

const articleSchema = new mongoose.Schema({
	code: String,
	designation: String,
	stock: Number,
	qte_en_commande: Number,
	qte_par_jeu: Number
});

const lancementSchema = new mongoose.Schema({
	numero_lancement: Number,
	article_code: String,
	designation: String,
	quantite: Number,
	etat: String

});

// ******* Creating models *******
const Article = mongoose.model('Article', articleSchema);
const Lancement = mongoose.model('Lancement', lancementSchema);



async function calculLancements() {
	try{
		let articles = await Article.find();
		let stockVirtuel = 0;
		const lcts = await Lancement.find();
		let compteur = lcts.length;
		console.log(compteur);
		for (const article of articles) {
			stockVirtuel = article.stock - article.qte_en_commande;
			if (stockVirtuel < 0) {
				console.log(article.code + ": stock virtuel : " + stockVirtuel);
				console.log("**** Recherche de lancement ****");
				const lancements = await Lancement.find({article_code: article.code});
				if (lancements.length > 0){
					console.log(lancements)
					for (const lancement of lancements) {
						stockVirtuel = stockVirtuel + lancement.quantite/article.qte_par_jeu;
					}
					
				}else{
					console.log("Pas de lancements en cours")
				}
				console.log("Stock virtuel après calcul: " + stockVirtuel);
			}
			if (stockVirtuel < 0){
				compteur++;
				let qteALancer = -1*stockVirtuel*article.qte_par_jeu
				const nouveauLancement = new Lancement({
					numero_lancement: compteur,
					article_code: article.code,
					designation: article.designation,
					quantite: qteALancer,
					etat: "en cours"
				})
				nouveauLancement.save();
				console.log("lancement " + compteur + " crée, article: " + article.code + " designation: " + article.designation + " qté: " + qteALancer);
			}
		}
	}catch(err){
		console.log(err);
	}
};

calculLancements();
