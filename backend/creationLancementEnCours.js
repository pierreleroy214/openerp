const express = require('express');
const mongoose = require('mongoose');
const fs = require('fs');

let rawdata = fs.readFileSync('lancementMorsEnCours26042022.json');
let lancements = JSON.parse(rawdata);


const app = express();
let port = 5000;

app.use(express.static('public'));

mongoose.connect('mongodb://localhost:27017/erp');

// ******* Defining Schemas *******

const articleSchema = new mongoose.Schema({
	code: String,
	designation: String,
	stock: Number,
	qte_en_commande: Number
//	dateDeCreation: Date,
//	createur: String,
//	type: String,
//	numeroDePlan: String,
//	indiceDePlan: String,
//	etatDeValiadtion: String,
//	uniteDeGestion: String,
//	quantiteEconimique: Number,
//	stockDeSecurite: Number,
//	prixDeVente: Number,
//	gammePrincipale: String
});

const lancementSchema = new mongoose.Schema({
	numero_lancement: Number,
	article_code: String,
	designation: String,
	quantite: Number,
	etat: String

});

// ******* Creating models *******
const Article = mongoose.model('Article', articleSchema);
const Lancement = mongoose.model('Lancement', lancementSchema);

let x = 1;
lancements.forEach(element => {
	const lancement = new Lancement({
		numero_lancement: x,
		article_code: element.code,
		designation: element.designation,
		quantite: element.qte,
		etat: "en cours"
	})
	lancement.save();
	console.log("lancement numero " + x + " crée");
	x = x+1;
})
console.log("fin de la création")


