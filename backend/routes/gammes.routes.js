const express = require('express')
const router = express.Router()
const Gamme = require('../controllers/gammes.ctrl')
const m = require('../helpers/middlewares')

router.post('/', m.checkContent, Gamme.saveOne);

router.get('/', Gamme.findAll);

router.get('/:id', m.checkId, Gamme.findById)

router.put('/:id', m.checkId, m.checkContent, Gamme.updateById)

router.delete('/:id', m.checkId, Gamme.deleteById)

module.exports = router
