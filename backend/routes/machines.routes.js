const express = require('express')
const router = express.Router()
const Machine = require('../controllers/machines.ctrl')
const m = require('../helpers/middlewares')

router.post('/', m.checkContent, Machine.saveOne);

router.get('/', Machine.findAll);

router.get('/:id', m.checkId, Machine.findById)

router.put('/:id', m.checkId, m.checkContent, Machine.updateById)

router.delete('/:id', m.checkId, Machine.deleteById)

module.exports = router
