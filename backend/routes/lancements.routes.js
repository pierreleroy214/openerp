const express = require('express')
const router = express.Router()
const Lancement = require('../controllers/lancements.ctrl')
const m = require('../helpers/middlewares')

router.post('/', m.checkContent, Lancement.saveOne);

router.get('/', Lancement.findAll);

router.get('/:id', m.checkId, Lancement.findById)

router.put('/:id', m.checkId, m.checkContent, Lancement.updateById)

router.delete('/:id', m.checkId, Lancement.deleteById)

module.exports = router
