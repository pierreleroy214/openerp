const express = require('express')
const router = express.Router()
const Article = require('../controllers/articles.ctrl')
const m = require('../helpers/middlewares')

router.post('/', m.checkContent, Article.saveOne);

router.get('/', Article.findAll);

router.get('/:id', m.checkId, Article.findById)

router.put('/:id', m.checkId, m.checkContent, Article.updateById)

router.delete('/:id', m.checkId, Article.deleteById)

module.exports = router
