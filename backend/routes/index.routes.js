const express = require('express')
const router = express.Router()

//Endpoint des articles
router.use('/articles', require('./articles.routes'))

//Endpoint des gammes
router.use('/gammes', require('./gammes.routes'))

//Endpoint des lancements
router.use('/lancements', require('./lancements.routes'))

//Endpoint des machines
router.use('/machines', require('./machines.routes'))

//Endpoint des matieres
router.use('/materials', require('./materials.routes'))

module.exports = router
