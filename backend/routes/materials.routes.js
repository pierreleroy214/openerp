const express = require('express')
const router = express.Router()
const Material = require('../controllers/materials.ctrl')
const m = require('../helpers/middlewares')

router.post('/', m.checkContent, Material.saveOne);

router.get('/', Material.findAll);

router.get('/:id', m.checkId, Material.findById)

router.put('/:id', m.checkId, m.checkContent, Material.updateById)

router.delete('/:id', m.checkId, Material.deleteById)

module.exports = router
